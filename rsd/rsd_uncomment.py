import csv
from collections import OrderedDict

f = open("C:\\Users\\User\\Documents\\Fall 2016\\Machine Learning\\project\\yelp_academic_dataset_review_.csv",
         encoding='latin-1')
reader = csv.reader(f)
labels = next(reader)
Spike_Dictionary = {}
Business = str(0)
Star_counter = 0
initial = False

for main_row in reader:
    if Business != main_row[labels.index('business_id')]:
        if initial != False:
            temperory_Star.sort()
            # print (temperory_Star)
            # print(Star)
            # print (Star_counter)

        if Star_counter > 10:

            if (Star_counter % 2 == 0):
                x = int(Star_counter / 2)
                Q2 = (float(temperory_Star[x - 1]) + float(temperory_Star[x])) / 2
                # print(Q2)
                if (x % 2 == 0):
                    y = int(x / 2)
                    Q1 = (float(temperory_Star[y]) + float(temperory_Star[y - 1])) / 2
                    Q3 = (float(temperory_Star[x + y - 1]) + float(temperory_Star[x + y])) / 2
                else:
                    y = int((x - 1) / 2)
                    Q1 = float(temperory_Star[y])
                    Q3 = float(temperory_Star[x + y])
            else:
                x = int((Star_counter - 1) / 2)
                Q2 = float(temperory_Star[x])
                # print (Q2)

                if (x % 2 == 0):
                    y = int(x / 2)
                    Q1 = (float(temperory_Star[y]) + float(temperory_Star[y - 1])) / 2
                    Q3 = (float(temperory_Star[x + y]) + float(temperory_Star[x + y + 1])) / 2
                else:
                    y = int((x - 1) / 2)
                    Q1 = float(temperory_Star[y])
                    Q3 = float(temperory_Star[x + y + 1])

            IQR = Q3 - Q1
            Outlier1 = Q3 + 1.5 * IQR
            # print(Outlier1)
            Outlier2 = Q1 - 1.5 * IQR
            # print(Outlier2)
            # print(Star)

            # Outlier1 = str(Outlier1)
            # Outlier2 = str(Outlier2)

            index_user = []
            i = 0

            for row in Star:
                if int(row) > Outlier1:
                    flag_spiky_business = True
                    index_user.append(i)
                elif int(row) < Outlier2:
                    flag_spiky_business = True
                    index_user.append(i)
                i += 1

            spiky_business_list = []

            if flag_spiky_business == True:
                spiky_business_list.append(Business)
                # print(index_user)
                spike_user = []

                for row in index_user:
                    spike_user.append(user[row])
                    Spike_Star.append(Star[row])
                    Spike_Review_ID.append(Review_ID[row])

                Spike_Dictionary[Business] = {}

                for row in range(len(index_user)):
                    Spike_Dictionary[Business]["User_Id" + str(row)] = spike_user[row]
                    Spike_Dictionary[Business]["User_Star" + str(row)] = Spike_Star[row]
                    Spike_Dictionary[Business]["User_Review_ID" + str(row)] = Spike_Review_ID[row]

                print(Spike_Dictionary)

        initial = True

        Business = main_row[labels.index('business_id')]
        print(Business)
        # if Business == '0zO219KZJf-5QsRNRMmELQ':
        # print("")

        Date = []
        Star = []
        temperory_Star = []
        user = []
        Review_ID = []
        Spike_Star = []
        Spike_Review_ID = []
        flag_spiky_business = False

        Star_counter = 0
        Review_ID.append(main_row[1])
        Date.append(main_row[7])
        # print (Date)
        Star.append(main_row[6])
        temperory_Star.append(main_row[6])
        Star_counter += 1
        user.append(main_row[0])

    else:
        Review_ID.append(main_row[1])
        Date.append(main_row[7])
        # print (Date)
        Star.append(main_row[6])
        temperory_Star.append(main_row[6])
        Star_counter += 1
        user.append(main_row[0])



        # print(user)
        # print(Star)

# print("Spiky User ID:", spike_user)
# Output1_file = open("C:\\Users\\User\\Documents\\Fall 2016\\Machine Learning\\project\\Spike_user_Id.csv","w")
# writer1 = csv.writer(Output1_file, delimiter = '\t')

# for row in spike_user:
#   writer1.writerow(row)

# print("Spiky Business ID:", spiky_business_list)
# Output2_file = open("C:\\Users\\User\\Documents\\Fall 2016\\Machine Learning\\project\\Spiky_business_Id.csv","w")
# writer2 = csv.writer(Output2_file, delimiter = '\t')

# for row in spiky_business_list:
#   writer2.writerow(row)

# Output2_file.close()
# Output1_file.close()
f.close()
