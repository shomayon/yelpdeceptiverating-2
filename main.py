#!/usr/bin/python -tt
# This is the main file of an entire application
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

from unitest import *
from popular import *
import filelocation

# Unit-test for each different module
def unit_test():
    # Unit-test for cluster
    # cluster_unittest.test()

    # Unit-test for determining popular users
    success = popular_unittest.user_test()

    # Unit-test for determining businesses rated by popular users and their trusty-score
    if success:
        popular_unittest.business_test()

def main():
    print 'Yelp Deceptive Rating'
    # Determine a list of popular users, a list of businesses rated by these users, and their trusty scores
    success = user.get_popular_users(filelocation.USER_FILE)
    '''if success:
        pop_bus, bus_trusty = business.get_business(filelocation.GEN_POP_UID, filelocation.REVIEW_FILE)
        print pop_bus
        print bus_trusty'''

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
    main()
