#!/usr/bin/python -tt
# This module extracts a set of popular users from Yelp Challenge DataSet
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

import pandas as pd
import numpy as np
import os.path, random, operator
from cluster import bic

# Feature columns from the data file
votes = ['votes.cool', 'votes.funny', 'votes.useful']
compliments = ['compliments.funny', 'compliments.list', 'compliments.photos', 'compliments.funny', 'compliments.plain',
               'compliments.cute', 'compliments.note', 'compliments.writer', 'compliments.hot', 'compliments.cool',
               'compliments.profile', 'compliments.more']


# Convert the format of yyyy-mm into decimal value
def parse_date(value):
    dates = value.split('-')
    return int(dates[0]) + int(dates[1]) / 100.


# Create a feature vector for each user
def featurize(row):
    yelping_since = parse_date(row['yelping_since'])
    average_stars = row['average_stars']
    elite_count = len(row['elite'])
    fans = row['fans']
    friends_count = len(row['friends'])
    review_count = row['review_count']
    total_votes = sum([row[v] for v in votes])
    total_compliments = sum([row[c] for c in compliments])
    return np.array(
        [yelping_since, average_stars, elite_count, fans, friends_count, review_count, total_votes, total_compliments])


# Prepare dataSet which is a list of (user_id, feature_vector)
def preprare_dataSet(fptr):
    dataSet = []
    print 'Extracting DataSet Feature: iterating through ...'
    for idx in fptr.index:
        print '  data row ', idx
        row = fptr.loc[idx,]
        user_id = row['user_id']
        features_vector = np.nan_to_num(featurize(row))
        dataSet.append((user_id, features_vector))
    print 'Extracting DataSet Feature: DONE'
    return dataSet


# Randomly initialize centroid feature vectors
def initialize_centroids(K):
    centroids = []
    random.seed(1)
    print 'Initializing centroids ...'
    for k in xrange(K):
        yelping_since = random.uniform(2004, 2016)
        average_stars = random.uniform(1, 5)
        elite_count = random.uniform(0, 12)
        fans = random.uniform(0, 600)
        friends_count = random.uniform(0, 1500)
        review_count = random.uniform(0, 2000)
        total_votes = random.uniform(0, 60000)
        total_compliments = random.uniform(0, 25000)
        centroids.append([yelping_since, average_stars, elite_count, fans, friends_count, review_count, total_votes,
                          total_compliments])
    print 'Initializing centroids: DONE'
    return np.array(centroids)


# Save all founded popular user_id into a text file
def save_uid_to_file(user_ids):
    print 'Writing out a list of popular users...',
    ''' Format of the output file:
            user_id_1
            user_id_2
            user_id_3
            ... '''
    of_stream = open('./gen/gen_pop_uid.txt', 'wb')
    for id in user_ids:
        of_stream.write(id + '\n')
    of_stream.close()
    print 'DONE'


# Helper print function
def print_stats(feature_stats, group, size):
    print '*** Cluster: ', group, ' *****************'
    print '       yelping_since: ', feature_stats[0]
    print '       average_stars: ', feature_stats[1]
    print '       elite_count: ', feature_stats[2]
    print '       fans: ', feature_stats[3]
    print '       friends_count: ', feature_stats[4]
    print '       review_count: ', feature_stats[5]
    print '       total_votes: ', feature_stats[6]
    print '       total_compliments: ', feature_stats[7]
    print '       total_users: ', size
    print '**********************************'


# Print out statistics of each clusters
def stats(clusters):
    for group, users in clusters.items():
        feature_stats = np.array([user[1] for user in users]).mean(axis=0)
        print_stats(feature_stats, group, len(users))


# bic-cluster to determine group of popular users
def get_popular_users(user_file_path):
    print '---------- DETERMINE POPULAR USERS ----------'
    fptr = None
    if os.path.exists(user_file_path):
        file_name = user_file_path.split('/')[-1:]
        print 'Reading file', file_name[0]
        fptr = pd.read_csv(user_file_path)
    else:
        print 'Error:', file_name[0], ' does not exist. Please check the file path'
        return

    MAX_ITERATIONS, k = 1000, 4
    dataSet = preprare_dataSet(fptr)
    centroids = initialize_centroids(k)
    print 'Starting to cluster users ...'
    clusters, centroids = bic(dataSet, centroids, MAX_ITERATIONS)
    print 'Starting to cluster users: DONE'
    sorted_clusters = sorted(clusters.items(), key=operator.itemgetter(1))
    user_ids = [tuple_value[0] for tuple_value in sorted_clusters[-1][1]]  # get the list of popular user_id

    save_uid_to_file(user_ids)
    stats(clusters)

    if len(user_ids) > 0: return True
    else: return False