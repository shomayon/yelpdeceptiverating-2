#!/usr/bin/python -tt
# This module is the unit test of Viet Trinh for BIC_clustering
__author__ = "Viet Trinh"  # vqtrinh@ucsc.edu

import random, string
import numpy as np
import matplotlib.pyplot as plt
from popular import *


def visualize(clusters, centroids):
    X, Y, cx, cy = [], [], [], []

    # get x-coord and y-coord of all data points
    for k in sorted(clusters.keys()):
        x, y = [], []
        [x.append(point[1][0]) for point in clusters[k]]
        [y.append(point[1][1]) for point in clusters[k]]
        X.append(x)
        Y.append(y)

    # get all x-coord and y-coord of centroids
    [cx.append(c[0]) for c in centroids]
    [cy.append(c[1]) for c in centroids]

    # plot clusters
    colorSet = ['red', 'green', 'blue', 'yellow', 'black', 'pink']
    for i in xrange(len(X)):
        plt.plot(X[i], Y[i], 'o', markersize=6, label='data', color=colorSet[i])
        plt.plot(cx[i], cy[i], 'x', markersize=10, label='data', color=colorSet[i])
    plt.show()


def test():
    random.seed(1)
    MAX_ITERATIONS = 50  # maximum number of iterations for clustering
    MIN, MAX = 10, 40  # range of each data point
    featureSize = 2  # each feature is a 2D vector
    num = 100  # dataSet has 100 points

    # Generate a dataSet of 100 points in the form
    # [(str,array),(str,array),(str,array),(str,array)...]
    dataSet, featureSet = [], []
    [featureSet.append([random.uniform(MIN, MAX) for _ in xrange(featureSize)]) for _ in xrange(num)]
    [dataSet.append((random.choice(string.letters) + random.choice(string.digits), np.array(feature))) for feature in
     featureSet]

    # Generate set of 4 random centroids
    centroids = np.array([[0, 0], [50, 0], [0, 50], [50, 50]])

    # Cluster and Visualize
    clusters, centroids = cluster.bic(dataSet, centroids, MAX_ITERATIONS)
    visualize(clusters, centroids)
